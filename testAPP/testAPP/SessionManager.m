//
//  SessionManager.h
//  testAPP
//
//  Created by Nikita Novikov on 29.10.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "SessionManager.h"
#import "Mapper.h"
#import "PostModel.h"


@interface SessionManager ()

@property (strong, nonatomic) Mapper *mapper;

@end

@implementation SessionManager

+ (SessionManager*) sharedManager {
    
    static SessionManager* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[SessionManager alloc] initSharedInstance];
    });
    return manager;
}

- (instancetype)initSharedInstance {
    NSURL *baseURL = [NSURL URLWithString:@"https://jsonplaceholder.typicode.com"];
    self = [super initWithBaseURL:baseURL];
    if (self) {
        self.requestSerializer = [AFJSONRequestSerializer new];
        self.responseSerializer = [AFJSONResponseSerializer new];
        self.mapper=[Mapper new];
    }
    return self;
}


#pragma mark - GET request

- (void)getPostForNumber:(NSUInteger)postNumber
			   onSuccess:(void(^)(PostModel* post))success
			   onFailure:(void(^)(NSError* error)) failure;
{
	if (postNumber > 100)
	{
		NSLog(@"postNumber can not be more then 100");
		if (failure)
		{
			failure(nil);
		}
		else
		{
			return;
		}
	}
	[self GET:[NSString stringWithFormat:@"posts/%lu",postNumber]
   parameters:nil
	 progress:nil
	  success:^(NSURLSessionDataTask * task, id  responseObject) {
		  if (success)
		  {
			  success([self.mapper postFromResponse:responseObject]);
		  }
	  } failure:^(NSURLSessionDataTask *task, NSError *error) {
		  NSLog(@"Error: %@", error);
		  if (failure)
		  {
			  failure(error);
		  }
	  }];
}

- (void)getCommentForNumber:(NSUInteger)postNumber
				  onSuccess:(void(^)(CommentModel *comment))success
				  onFailure:(void(^)(NSError* error)) failure;
{
	if (postNumber > 100)
	{
		NSLog(@"commentNumber can not be more then 500");
		if (failure)
		{
			failure(nil);
		}
		else
		{
			return;
		}
	}
	[self GET:[NSString stringWithFormat:@"comments/%lu",postNumber]
   parameters:nil
	 progress:nil
	  success:^(NSURLSessionDataTask * task, id  responseObject) {
		  if (success)
		  {
			  success([self.mapper commentFromResponse:responseObject]);
		  }
	  } failure:^(NSURLSessionDataTask *task, NSError *error) {
		  NSLog(@"Error: %@", error);
		  if (failure)
		  {
			  failure(error);
		  }
	  }];
}

- (void)getUsersOnSuccess:(void(^)(NSArray <UserModel* >*users))success
				onFailure:(void(^)(NSError* error)) failure
{
	NSMutableArray *users = [NSMutableArray new];
	for (NSUInteger i = 1; i <= 5; i++)
	{
		[self GET:[NSString stringWithFormat:@"users/%lu",i]
	   parameters:nil
		 progress:nil
		  success:^(NSURLSessionDataTask * task, id  responseObject) {
			  [users addObject:[self.mapper userFromResponse:responseObject]];
			  if (success && i == 5)
			  {
				  success(users);
			  }
		  } failure:^(NSURLSessionDataTask *task, NSError *error) {
			  NSLog(@"Error: %@", error);
			  if (failure)
			  {
				  failure(error);
			  }
		  }];
	}
}

- (void)getURLPictureOnSuccess:(void(^)(NSString *URLString))success
					 onFailure:(void(^)(NSError* error)) failure
{
	[self GET:[NSString stringWithFormat:@"photos/3"]
   parameters:nil
	 progress:nil
	  success:^(NSURLSessionDataTask * task, id  responseObject) {
		  if (success)
		  {
			  success(responseObject[@"url"]);
		  }
	  } failure:^(NSURLSessionDataTask *task, NSError *error) {
		  NSLog(@"Error: %@", error);
		  if (failure)
		  {
			  failure(error);
		  }
	  }];
}

- (void)getTODOonSuccess:(void(^)(TODOModel *TODO))success
			   onFailure:(void(^)(NSError* error)) failure
{
	NSUInteger randomTODO = (arc4random()%199) + 1;
	[self GET:[NSString stringWithFormat:@"todos/%lu",randomTODO]
   parameters:nil
	 progress:nil
	  success:^(NSURLSessionDataTask * task, id  responseObject) {
		  if (success)
		  {
			  success([self.mapper TODOFromResponse:responseObject]);
		  }
	  } failure:^(NSURLSessionDataTask *task, NSError *error) {
		  NSLog(@"Error: %@", error);
		  if (failure)
		  {
			  failure(error);
		  }
	  }];
}

@end
