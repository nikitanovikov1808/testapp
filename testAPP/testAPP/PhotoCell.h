//
//  PhotoCell.h
//  testAPP
//
//  Created by Nikita Novikov on 31.10.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photoImage;

@end
