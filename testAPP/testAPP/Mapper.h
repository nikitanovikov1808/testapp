//
//  Mapper.h
//  testAPP
//
//  Created by Nikita Novikov on 29.10.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PostModel;
@class CommentModel;
@class UserModel;
@class TODOModel;


@interface Mapper : NSObject

- (PostModel *)postFromResponse:(NSDictionary *)responseObject;

- (CommentModel *)commentFromResponse:(NSDictionary *)responseObject;

- (UserModel *)userFromResponse:(NSDictionary *)responseObject;

- (TODOModel *)TODOFromResponse:(NSDictionary *)responseObject;

@end
