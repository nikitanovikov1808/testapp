//
//  CommentCell.m
//  testAPP
//
//  Created by Nikita Novikov on 31.10.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "CommentCell.h"
#import "SessionManager.h"
#import "CommentModel.h"

@implementation CommentCell

- (void)awakeFromNib
{
    [super awakeFromNib];
	self.layer.cornerRadius = 25;
}

- (IBAction)DidAccept:(id)sender
{
	[self.numberTextField resignFirstResponder];
	if (!self.numberTextField.text.integerValue)
	{
		self.textView.text = @"Введите номер от 1 до 100";
		return;
	}
	[[SessionManager sharedManager] getCommentForNumber:self.numberTextField.text.integerValue
	  onSuccess:^(CommentModel *comment) {
		  self.textView.text = [NSString stringWithFormat:@"NAME: %@\nEMAIL: %@\nTEXT: %@",comment.name, comment.email, comment.body];
	  } onFailure:^(NSError *error) {
		  self.textView.text = [NSString stringWithFormat:@"ERROR!\nCode: %ld", (long)error.code];
	  }];
}

@end
