//
//  TODOCell.m
//  testAPP
//
//  Created by Nikita Novikov on 31.10.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "TODOCell.h"
#import "TODOModel.h"
#import "SessionManager.h"

@implementation TODOCell

- (void)awakeFromNib{
	self.layer.cornerRadius = 25;
    [super awakeFromNib];
	[[SessionManager sharedManager] getTODOonSuccess:^(TODOModel *TODO) {
		self.TODOLabel.text = TODO.title;
		self.swither.on = TODO.completed;
	} onFailure:^(NSError *error) {
		self.TODOLabel.text = @"ERROR: can not get TODO";
	}];
}

@end
