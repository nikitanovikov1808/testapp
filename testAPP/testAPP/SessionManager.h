//
//  SessionManager.h
//  testAPP
//
//  Created by Nikita Novikov on 29.10.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@class PostModel;
@class CommentModel;
@class UserModel;
@class TODOModel;


@interface SessionManager : AFHTTPSessionManager

+ (SessionManager*) sharedManager;


- (void)getPostForNumber:(NSUInteger)postNumber
			   onSuccess:(void(^)(PostModel* post))success
			   onFailure:(void(^)(NSError* error)) failure;

- (void)getCommentForNumber:(NSUInteger)postNumber
				  onSuccess:(void(^)(CommentModel *comment))success
				  onFailure:(void(^)(NSError* error)) failure;

- (void)getUsersOnSuccess:(void(^)(NSArray <UserModel* >*users))success
				onFailure:(void(^)(NSError* error)) failure;

- (void)getURLPictureOnSuccess:(void(^)(NSString *URLString))success
					 onFailure:(void(^)(NSError* error)) failure;

- (void)getTODOonSuccess:(void(^)(TODOModel *TODO))success
			   onFailure:(void(^)(NSError* error)) failure;

@end
