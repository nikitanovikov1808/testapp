//
//  CommentModel.h
//  testAPP
//
//  Created by Nikita Novikov on 30.10.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *body;
@property (nonatomic, copy) NSString *email;

@end
