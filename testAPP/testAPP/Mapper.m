//
//  Mapper.m
//  testAPP
//
//  Created by Nikita Novikov on 29.10.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "Mapper.h"
#import "PostModel.h"
#import "CommentModel.h"
#import "UserModel.h"
#import "TODOModel.h"


@implementation Mapper

- (PostModel *)postFromResponse:(NSDictionary *)responseObject
{
	PostModel *post = [PostModel new];
	
	post.title = responseObject[@"title"] ?:@"";
	post.body = responseObject[@"body"] ?:@"";
	
	return post;
}

- (CommentModel *)commentFromResponse:(NSDictionary *)responseObject
{
	CommentModel *comment = [CommentModel new];
	
	comment.body = responseObject[@"body"] ?:@"";
	comment.name = responseObject[@"name"] ?:@"";
	comment.email = responseObject[@"email"] ?:@"";

	return comment;
}

- (UserModel *)userFromResponse:(NSDictionary *)responseObject
{
	UserModel *user = [UserModel new];
	user.name = responseObject[@"name"] ?:@"";
	return user;
}

- (TODOModel *)TODOFromResponse:(NSDictionary *)responseObject
{
	TODOModel *TODO = [TODOModel new];
	TODO.title = responseObject[@"title"] ?:@"";
	if (responseObject[@"completed"])
	{
		TODO.completed = YES;
	}
	else
	{
		TODO.completed = NO;
	}
	return TODO;
}

@end
