//
//  UsersCell.m
//  testAPP
//
//  Created by Nikita Novikov on 31.10.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//


#import "UsersCell.h"
#import "SessionManager.h"
#import "UserModel.h"


@implementation UsersCell

- (void)awakeFromNib
{
    [super awakeFromNib];
	self.layer.cornerRadius = 25;
	[[SessionManager sharedManager] getUsersOnSuccess:^(NSArray<UserModel *> *users) {
		if (users.count == 5)
		{
			self.User1.text = users[0].name;
			self.User2.text = users[1].name;
			self.User3.text = users[2].name;
			self.User4.text = users[3].name;
			self.User5.text = users[4].name;
		}
		else
		{
			self.User1.text = @"ERROR: can not get all users";
		}
	} onFailure:^(NSError *error) {
		self.User1.text = @"ERROR: can not get users";
	}];
}

@end
