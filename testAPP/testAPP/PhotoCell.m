//
//  PhotoCell.m
//  testAPP
//
//  Created by Nikita Novikov on 31.10.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "PhotoCell.h"
#import "SessionManager.h"

@implementation PhotoCell

- (void)awakeFromNib {
    [super awakeFromNib];
	self.layer.cornerRadius = 25;
    [[SessionManager sharedManager] getURLPictureOnSuccess:^(NSString *URLString) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
			NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:URLString]];
			UIImage *image = [UIImage imageWithData:data];
			dispatch_async(dispatch_get_main_queue(), ^{
				self.photoImage.image = image;
			});
		});
	} onFailure:^(NSError *error) {
		return;
	}];
}

@end
