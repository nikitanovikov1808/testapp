//
//  TODOModel.h
//  testAPP
//
//  Created by Nikita Novikov on 30.10.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TODOModel : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) BOOL completed;

@end
