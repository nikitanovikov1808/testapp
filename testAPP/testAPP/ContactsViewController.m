//
//  ContactsViewController.m
//  testAPP
//
//  Created by Nikita Novikov on 29.10.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "ContactsViewController.h"
#import <Contacts/Contacts.h>
#import "SessionManager.h"


@interface ContactsViewController () <UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray <NSDictionary *> *contacts;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ContactsViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.contacts = [NSMutableArray new];
	[self getContacts];
	[self.tableView reloadData];
}


#pragma mark - getContacts

- (void)getContacts
{
	if ([CNContactStore class])
	{
		CNEntityType entityType = CNEntityTypeContacts;
		if( [CNContactStore authorizationStatusForEntityType:entityType] == CNAuthorizationStatusNotDetermined)
		{
			CNContactStore * contactStore = [[CNContactStore alloc] init];
			[contactStore requestAccessForEntityType:entityType
								   completionHandler:^(BOOL granted, NSError * _Nullable error){
				if(granted)
				{
					[self getAllContacts];
				}
			}];
		}
		
		else if( [CNContactStore authorizationStatusForEntityType:entityType] == CNAuthorizationStatusAuthorized)
		{
			[self getAllContacts];
		}
		else
		{
			[self showError];
		}
	}
}

-(void)getAllContacts
{
	if([CNContactStore class])
	{
		NSError* contactError;
		CNContactStore* addressBook = [[CNContactStore alloc] init];
		
		[addressBook containersMatchingPredicate:[CNContainer predicateForContainersWithIdentifiers: @[addressBook.defaultContainerIdentifier]] error:&contactError];
		
		NSArray * keysToFetch =@[CNContactPhoneNumbersKey, CNContactFamilyNameKey, CNContactGivenNameKey];
		CNContactFetchRequest * request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keysToFetch];
		
		BOOL success = [addressBook enumerateContactsWithFetchRequest:request
																error:&contactError
														   usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop){
			[self parseContactWithContact:contact];
		}];
		if (!success)
		{
			dispatch_async(dispatch_get_main_queue(), ^{
				[self showError];
			});
		}
		else
		{
			dispatch_async(dispatch_get_main_queue(), ^{
				[self configureTableView];
			});
		}
	}
}

- (void)parseContactWithContact :(CNContact* )contact
{
	NSString *firstName = contact.givenName ?:@"";
	NSString *lastName = contact.familyName ?:@"";
	NSString *name = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
	NSArray *phones = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
	NSString *phone = [NSString stringWithFormat:@"%@", [phones firstObject]];
	NSDictionary *contactInfo = @{@"name" : name,
								 @"phone" : phone,
								 };
	
	[self.contacts addObject:contactInfo];
}


#pragma mark - TableView

- (void)configureTableView
{
	self.tableView.dataSource = self;
	[self.tableView reloadData];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.contacts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contactsCell" forIndexPath:indexPath];
	NSDictionary *contactInfo = [self.contacts objectAtIndex:indexPath.row];
	cell.textLabel.text = contactInfo[@"name"];
	cell.detailTextLabel.text = contactInfo[@"phone"];
	return cell;
}


#pragma mark - Error

- (void)showError
{
	UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ошибка" message:@"Ошибка получения контактов. Необходимая версия iOS должна быть >= 9.0 версии" preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction *exitButton = [UIAlertAction actionWithTitle:@"Выйти" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
		exit(0);
	}];
	
	UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Попробовать еще раз" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
		[self getContacts];
	}];
	
	[alert addAction:exitButton];
	[alert addAction:cancelAction];
	[self presentViewController:alert animated:YES completion:nil];
}

@end
