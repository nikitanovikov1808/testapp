//
//  UserModel.h
//  testAPP
//
//  Created by Nikita Novikov on 30.10.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject

@property (nonatomic, copy) NSString *name;

@end
