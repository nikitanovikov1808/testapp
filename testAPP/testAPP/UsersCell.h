//
//  UsersCell.h
//  testAPP
//
//  Created by Nikita Novikov on 31.10.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UsersCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *User1;
@property (weak, nonatomic) IBOutlet UILabel *User2;
@property (weak, nonatomic) IBOutlet UILabel *User3;
@property (weak, nonatomic) IBOutlet UILabel *User4;
@property (weak, nonatomic) IBOutlet UILabel *User5;

@end
