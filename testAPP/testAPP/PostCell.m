//
//  PostAndCommentCell.m
//  testAPP
//
//  Created by Nikita Novikov on 30.10.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//

#import "PostCell.h"
#import "SessionManager.h"
#import "PostModel.h"

@implementation PostCell

- (void)awakeFromNib {
    [super awakeFromNib];
	self.layer.cornerRadius = 25;
}

- (IBAction)DidAccept:(id)sender
{
	[self.numberTextField resignFirstResponder];
	if (!self.numberTextField.text.integerValue)
	{
		self.textView.text = @"Введите номер от 1 до 100";
		return;
	}
	// of couse I should use weakify/strongify self in block
	[[SessionManager sharedManager] getPostForNumber:self.numberTextField.text.integerValue onSuccess:^(PostModel *post) {
		self.textView.text = [NSString stringWithFormat:@"TITLE: %@\nTEXT:%@", post.title, post.body];
	} onFailure:^(NSError *error) {
		self.textView.text = [NSString stringWithFormat:@"ERROR!\nCode: %ld", (long)error.code];
	}];
}


@end
