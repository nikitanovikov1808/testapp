//
//  CardsViewController.m
//  testAPP
//
//  Created by Nikita Novikov on 30.10.17.
//  Copyright © 2017 BMSTU. All rights reserved.
//


#import "CardsViewController.h"
#import "PostCell.h"
#import "CommentCell.h"
#import "TODOCell.h"
#import "PhotoCell.h"


static NSString *postReuseIdentifier =  @"postReuseIdentifier";
static NSString *commentReuseIdentifier =  @"commentReuseIdentifier";
static NSString *usersReuseIdentifier =  @"usersReuseIdentifier";
static NSString *TODOReuseIdentifier =  @"TODOReuseIdentifier";
static NSString *photoReuseIdentifier =  @"photoReuseIdentifier";

CGFloat const cardViewWidth = 300.0;
CGFloat const cardViewHeight = 400.0;
CGFloat const cardLineSpacing = 20.0;

@interface CardsViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, assign) NSUInteger selectedIndex;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation CardsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self createLayoutCollectionView];
	[self registerAllXibs];
	self.selectedIndex = 0;
	self.imageView.image = [UIImage imageNamed:@"1.jpg" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
}

- (void)createLayoutCollectionView
{
	UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
	CGFloat screenWidth = CGRectGetWidth(self.view.frame);
	CGFloat offsetX = (screenWidth-300) / 2;
	layout.sectionInset = UIEdgeInsetsMake(0, offsetX, 0, offsetX);
	layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
	self.collectionView.collectionViewLayout = layout;
	self.collectionView.decelerationRate = UIScrollViewDecelerationRateFast;
	self.collectionView.userInteractionEnabled = YES;
	self.collectionView.scrollEnabled = YES;
	self.collectionView.clipsToBounds = YES;
	self.collectionView.showsVerticalScrollIndicator = NO;
	self.collectionView.showsHorizontalScrollIndicator = NO;
	self.collectionView.delegate = self;
	self.collectionView.dataSource = self;
}

- (void)registerAllXibs
{
	UINib *xibPostCell = [UINib nibWithNibName:@"PostCell" bundle:nil];
	[self.collectionView registerNib:xibPostCell forCellWithReuseIdentifier:postReuseIdentifier];
	
	UINib *xibCommentCell = [UINib nibWithNibName:@"CommentCell" bundle:nil];
	[self.collectionView registerNib:xibCommentCell forCellWithReuseIdentifier:commentReuseIdentifier];
	
	UINib *xibUsersCell = [UINib nibWithNibName:@"UsersCell" bundle:nil];
	[self.collectionView registerNib:xibUsersCell forCellWithReuseIdentifier:usersReuseIdentifier];
	
	UINib *xibTODOCell = [UINib nibWithNibName:@"TODOCell" bundle:nil];
	[self.collectionView registerNib:xibTODOCell forCellWithReuseIdentifier:TODOReuseIdentifier];
	
	UINib *xibPhotoCell = [UINib nibWithNibName:@"PhotoCell" bundle:nil];
	[self.collectionView registerNib:xibPhotoCell forCellWithReuseIdentifier:photoReuseIdentifier];
}

#pragma mark - UICollectionViewDataSource


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return 5;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row == 0)
	{
		UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:postReuseIdentifier forIndexPath:indexPath];
		return cell;
	}
	else if (indexPath.row == 1)
	{
		UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:commentReuseIdentifier forIndexPath:indexPath];
		return cell;
	}
	else if (indexPath.row == 2)
	{
		UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:usersReuseIdentifier forIndexPath:indexPath];
		return cell;
	}
	else if (indexPath.row == 3)
	{
		UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:TODOReuseIdentifier forIndexPath:indexPath];
		return cell;
	}
	else
	{
		UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:photoReuseIdentifier forIndexPath:indexPath];
		return cell;
	}
}


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
	return CGSizeMake(cardViewWidth, cardViewHeight);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
	return cardLineSpacing;
}


#pragma mark - UICollectionViewDelegate


- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
	if (!targetContentOffset)
	{
		return;
	}
	
	NSUInteger nearestPage = rintf(targetContentOffset->x / (cardViewWidth + cardLineSpacing));
	self.selectedIndex = MIN(nearestPage, 4);
	
	CGFloat nearestPageOffsetX = nearestPage * (cardViewWidth + cardLineSpacing);
	targetContentOffset->x = nearestPageOffsetX;
}

- (void)setSelectedIndex:(NSUInteger)newIndex
{
	if (newIndex < 5)
	{
		_selectedIndex = newIndex;
		self.pageControl.currentPage = newIndex;
		if (newIndex == 0)
		{
			self.imageView.image = [UIImage imageNamed:@"1.jpg" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
		}
		if (newIndex == 1)
		{
			self.imageView.image = [UIImage imageNamed:@"2.jpg" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
		}
		if (newIndex == 2)
		{
			self.imageView.image = [UIImage imageNamed:@"3.jpg" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
		}
		if (newIndex == 3)
		{
			self.imageView.image = [UIImage imageNamed:@"4.jpg" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
		}
		if (newIndex == 4)
		{
			self.imageView.image = [UIImage imageNamed:@"5.jpg" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
		}
	}
}

@end
